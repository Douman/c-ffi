use c_ffi::Args;

#[test]
fn verify_args() {
    let input = [
        "first\0",
        "second\0",
        "third\0",
        "fourth\0",
    ];

    let input_ptrs = [
        input[0].as_ptr(),
        input[1].as_ptr(),
        input[2].as_ptr(),
        input[3].as_ptr(),
    ];

    let args = unsafe {
        Args::new(input_ptrs.len() as isize, input_ptrs.as_ptr()).expect("Successfully create args")
    };

    for (idx, arg) in args.into_iter().enumerate() {
        let expected = input[idx];
        let expected = &expected[..expected.len()-1];
        assert_eq!(expected, arg);
    }
}

#[should_panic]
#[test]
fn args_must_fail_validate_null_ptr() {
    unsafe {
        let _ = Args::new(5, core::ptr::null());
    }
}

#[should_panic]
#[test]
fn args_must_fail_validate_zero() {
    let test = [];
    unsafe {
        let _ =Args::new(0, test.as_ptr());
    }
}
