#[test]
fn verify_strlen() {
    let text = "waifu\0";
    let expected_size = text.len() - 1;
    let text = text.as_ptr() as *const i8;

    assert_eq!(expected_size, unsafe { c_ffi::strlen(text) });
}
