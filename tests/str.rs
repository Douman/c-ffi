use c_ffi::c_lit;

#[test]
fn verify_c_lit() {
    assert_eq!(c_lit!("hello", " ", "lolka"), "hello lolka\0");
}
