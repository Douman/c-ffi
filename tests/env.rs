#[test]
fn verify_get_var() {
    let expected = std::env::var("PATH").expect("To get PATH from std");
    let result = c_ffi::env::get_var("PATH").expect("To have PATH from ffi");

    assert_eq!(result, expected);
}

#[test]
fn verify_set_var() {
    const VAR_NAME: &str = "LOLKA";
    const VAR_VALUE: &str = "11111";

    assert!(c_ffi::env::set_var(VAR_NAME, VAR_VALUE));

    let expected = std::env::var(VAR_NAME).expect("To get VAR_NAME from std");
    assert_eq!(expected, VAR_VALUE);

    //TODO: On windows getenv is taking from constant environment
    //      While set_var uses SetEnvironmentVariableW which modifies mutable table.
    #[cfg(not(windows))]
    {
        let result = c_ffi::env::get_var(VAR_NAME).expect("To have VAR_NAME from ffi");

        assert_eq!(result, expected);
    }
}

#[test]
fn verify_unset_var() {
    const VAR_NAME: &str = "PATH";

    assert!(c_ffi::env::unset_var(VAR_NAME));

    assert!(std::env::var(VAR_NAME).is_err());

    #[cfg(not(windows))]
    {
        assert!(c_ffi::env::get_var(VAR_NAME).is_err());
    }
}
