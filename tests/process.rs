#[cfg(feature = "libc")]
#[test]
fn verify_system() {
    assert_eq!(c_ffi::system!("c", "d"), 0);
    assert_eq!(c_ffi::system!("cd"), 0);
}
