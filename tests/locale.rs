
#[test]
fn verify_locale() {
    #[cfg(windows)]
    const LOCALE: &str = "en_US.UTF-8";
    #[cfg(not(windows))]
    const LOCALE: &str = "C.UTF-8";

    let locale = c_ffi::locale::get().expect("To get locale");
    //default
    assert_eq!("C", locale);

    assert!(c_ffi::locale::set(LOCALE));

    let locale = c_ffi::locale::get().expect("To get locale");
    assert_eq!(LOCALE, locale);
}
