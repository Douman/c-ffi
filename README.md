# c-ffi

[![Build](https://gitlab.com/Douman/c-ffi/badges/master/pipeline.svg)](https://gitlab.com/Douman/c-ffi/pipelines)
[![Crates.io](https://img.shields.io/crates/v/c-ffi.svg)](https://crates.io/crates/c-ffi)
[![Documentation](https://docs.rs/c-ffi/badge.svg)](https://docs.rs/crate/c-ffi/)

My utilities for C FFI
