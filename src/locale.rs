//!Locale module
//!
//!## Required features:
//!
//! - `libc`

use core::{mem, ptr, cmp};

///Retrieves current locale in C
pub fn get() -> Result<&'static str, core::str::Utf8Error> {
    let result = unsafe {
        libc::setlocale(libc::LC_ALL, ptr::null())
    };

    assert!(!result.is_null());


    unsafe {
        crate::c_str_to_rust(result as *const u8)
    }
}

const LOCALE_LEN: usize = 128;

///Sets default C locale as `LC_ALL`
///
///Locale is limited to 128 characters
pub fn set(name: &str) -> bool {
    assert!(name.len() <= LOCALE_LEN);

    let mut name_buff = mem::MaybeUninit::<[i8; LOCALE_LEN + 1]>::uninit();
    let len = cmp::min(LOCALE_LEN, name.len());

    unsafe {
        let name_ptr = name_buff.as_mut_ptr() as *mut i8;
        ptr::copy_nonoverlapping(name.as_ptr() as *const i8, name_ptr, len);
        ptr::write(name_ptr.add(len), 0);
    }

    let result = unsafe {
        libc::setlocale(libc::LC_ALL, name_buff.as_ptr() as *const i8)
    };

    !result.is_null()
}
