#[cfg(feature = "libc")]
mod stdlib;
#[cfg(feature = "libc")]
pub use stdlib::*;

#[cfg(not(feature = "libc"))]
mod non_stdlib;
#[cfg(not(feature = "libc"))]
pub use non_stdlib::*;
