#[no_mangle]
///Simple Rust implementation of C strlen
pub unsafe extern "C" fn strlen(string: *const i8) -> usize {
    debug_assert!(!string.is_null());
    let mut res = 0;

    while *string.add(res) != 0 {
        res += 1;
    }

    res
}
