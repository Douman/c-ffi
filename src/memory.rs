//! Memory related utilities
//!
//!## Required features:
//!
//! - `memory`

///Wrapper for `libc::free`
pub struct FreeFn;

impl smart_ptr::Deleter for FreeFn {
    fn delete<T>(&mut self, ptr: *mut u8) {
        unsafe {
            libc::free(ptr as *mut _);
        }
    }
}

///Alias to unique ptr, suitable for malloc
pub type Box = smart_ptr::Unique<libc::c_void, FreeFn>;

///Extension to Unique for use with malloc
pub trait Malloc: Sized {
    ///Creates new memory using `malloc`
    ///
    ///# Panics:
    ///
    ///- When `malloc` returns 0
    fn malloc(size: usize) -> Self;
}

impl Malloc for Box {
    fn malloc(size: usize) -> Self {
        let ptr = unsafe {
            libc::malloc(size)
        };

        if ptr.is_null() {
            panic!("libc::malloc failed");
        }


        unsafe {
            Self::new(ptr, FreeFn)
        }
    }
}
