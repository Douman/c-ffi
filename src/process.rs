//! Process sub-module
//!
//!## Required features:

///Runs command, assuming string contains null character at the end.
///
///It is UB to pass string without (use macro `c_lit` to create such string)
pub unsafe fn system_unchecked(cmd: &str) -> i32 {
    crate::sys::system(cmd.as_ptr() as *const _)

}

///Creates command with `c_lit` macro and pass it to system call
#[macro_export]
macro_rules! system {
    ($e:expr) => {
        unsafe {
            $crate::process::system_unchecked($crate::c_lit!($e))
        }
    };
    ($($e:tt)+) => {
        unsafe {
            $crate::process::system_unchecked($crate::c_lit!($($e)+))
        }
    };
}
